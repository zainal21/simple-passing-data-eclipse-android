package com.example.formsiswa;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class HomeActivity extends Activity {
	TextView showInformasi;
	@Override
	
	
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		Intent intGoToHome = getIntent();
		String[] message = intGoToHome.getStringArrayExtra(MainActivity.Msg);
		showInformasi = (TextView)findViewById(R.id.homemsg);
		showInformasi.setText(
				"Nama \t : " + message[0].toString() +
				   "\n NIS \t : " + message[1].toString() +
				   "\n Tempat Lahir \t : " + message[2].toString() +
				   "\n Tanggal Lahir \t : " + message[3].toString());
	}
	
	public void BackMain(View view){
		Intent intBackMain = new Intent(this,MainActivity.class);
		startActivity(intBackMain);
	}
	
	
}
